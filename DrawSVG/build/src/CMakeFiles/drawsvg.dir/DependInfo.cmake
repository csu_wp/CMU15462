# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wp/Course/cg/DrawSVG/src/drawsvg.cpp" "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg.dir/drawsvg.cpp.o"
  "/home/wp/Course/cg/DrawSVG/src/main.cpp" "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg.dir/main.cpp.o"
  "/home/wp/Course/cg/DrawSVG/src/png.cpp" "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg.dir/png.cpp.o"
  "/home/wp/Course/cg/DrawSVG/src/software_renderer.cpp" "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg.dir/software_renderer.cpp.o"
  "/home/wp/Course/cg/DrawSVG/src/svg.cpp" "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg.dir/svg.cpp.o"
  "/home/wp/Course/cg/DrawSVG/src/texture.cpp" "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg.dir/texture.cpp.o"
  "/home/wp/Course/cg/DrawSVG/src/triangulation.cpp" "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg.dir/triangulation.cpp.o"
  "/home/wp/Course/cg/DrawSVG/src/viewport.cpp" "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg.dir/viewport.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../CMU462/include"
  "../CMU462/include/CMU462"
  "/usr/include/freetype2"
  "/usr/include/x86_64-linux-gnu/freetype2"
  "../src"
  "../CMU462/deps/glfw/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg_hdwr.dir/DependInfo.cmake"
  "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/DependInfo.cmake"
  "/home/wp/Course/cg/DrawSVG/build/CMU462/deps/glew/CMakeFiles/glew.dir/DependInfo.cmake"
  "/home/wp/Course/cg/DrawSVG/build/CMU462/deps/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
