# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wp/Course/cg/DrawSVG/src/hardware/hardware_renderer.cpp" "/home/wp/Course/cg/DrawSVG/build/src/CMakeFiles/drawsvg_hdwr.dir/hardware/hardware_renderer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../CMU462/include"
  "../CMU462/include/CMU462"
  "/usr/include/freetype2"
  "/usr/include/x86_64-linux-gnu/freetype2"
  "../src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
