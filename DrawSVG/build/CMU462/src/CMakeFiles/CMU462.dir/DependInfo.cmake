# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wp/Course/cg/DrawSVG/CMU462/src/base64.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/base64.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/color.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/color.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/complex.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/complex.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/lodepng.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/lodepng.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/matrix3x3.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/matrix3x3.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/matrix4x4.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/matrix4x4.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/osdfont.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/osdfont.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/osdtext.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/osdtext.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/quaternion.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/quaternion.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/spectrum.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/spectrum.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/tinyxml2.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/tinyxml2.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/vector2D.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/vector2D.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/vector3D.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/vector3D.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/vector4D.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/vector4D.cpp.o"
  "/home/wp/Course/cg/DrawSVG/CMU462/src/viewer.cpp" "/home/wp/Course/cg/DrawSVG/build/CMU462/src/CMakeFiles/CMU462.dir/viewer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../CMU462/include"
  "../CMU462/include/CMU462"
  "../CMU462/deps/glew/include"
  "/usr/include/freetype2"
  "/usr/include/x86_64-linux-gnu/freetype2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wp/Course/cg/DrawSVG/build/CMU462/deps/glew/CMakeFiles/glew.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
