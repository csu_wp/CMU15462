#include "software_renderer.h"

#include <cmath>
#include <vector>
#include <iostream>
#include <algorithm>

#include "triangulation.h"
using namespace std;

namespace CMU462 {


// Implements SoftwareRenderer //

void SoftwareRendererImp::draw_svg( SVG& svg ) {

  // set top level transformation
  transformation = canvas_to_screen;

  // draw all elements
  for ( size_t i = 0; i < svg.elements.size(); ++i ) {
    draw_element(svg.elements[i]);
  }

  // draw canvas outline
  Vector2D a = transform(Vector2D(    0    ,     0    )); a.x--; a.y--;
  Vector2D b = transform(Vector2D(svg.width,     0    )); b.x++; b.y--;
  Vector2D c = transform(Vector2D(    0    ,svg.height)); c.x--; c.y++;
  Vector2D d = transform(Vector2D(svg.width,svg.height)); d.x++; d.y++;

  rasterize_line(a.x, a.y, b.x, b.y, Color::Black);
  rasterize_line(a.x, a.y, c.x, c.y, Color::Black);
  rasterize_line(d.x, d.y, b.x, b.y, Color::Black);
  rasterize_line(d.x, d.y, c.x, c.y, Color::Black);

  // resolve and send to render target
  resolve();

}

void SoftwareRendererImp::set_sample_rate( size_t sample_rate ) {

  // Task 4: 
  // You may want to modify this for supersampling support
  this->sample_rate = sample_rate;

}

void SoftwareRendererImp::set_render_target( unsigned char* render_target,
                                             size_t width, size_t height ) {

  // Task 4: 
  // You may want to modify this for supersampling support
  this->render_target = render_target;
  this->target_w = width;
  this->target_h = height;

}

void SoftwareRendererImp::draw_element( SVGElement* element ) {

  // Task 5 (part 1):
  // Modify this to implement the transformation stack
  

	Matrix3x3 transformationTemp = transformation;	
	transformation = transformation * element->transform;
  switch(element->type) {
    case POINT:
      draw_point(static_cast<Point&>(*element));
      break;
    case LINE:
      draw_line(static_cast<Line&>(*element));
      break;
    case POLYLINE:
      draw_polyline(static_cast<Polyline&>(*element));
      break;
    case RECT:
      draw_rect(static_cast<Rect&>(*element));
      break;
    case POLYGON:
      draw_polygon(static_cast<Polygon&>(*element));
      break;
    case ELLIPSE:
      draw_ellipse(static_cast<Ellipse&>(*element));
      break;
    case IMAGE:
      draw_image(static_cast<Image&>(*element));
      break;
    case GROUP:
      draw_group(static_cast<Group&>(*element));
      break;
    default:
      break;
  }

	transformation = transformationTemp;
}


// Primitive Drawing //

void SoftwareRendererImp::draw_point( Point& point ) {

  Vector2D p = transform(point.position);
  rasterize_point( p.x, p.y, point.style.fillColor );

}

void SoftwareRendererImp::draw_line( Line& line ) { 

  Vector2D p0 = transform(line.from);
  Vector2D p1 = transform(line.to);
  rasterize_line( p0.x, p0.y, p1.x, p1.y, line.style.strokeColor );

}

void SoftwareRendererImp::draw_polyline( Polyline& polyline ) {

  Color c = polyline.style.strokeColor;

  if( c.a != 0 ) {
    int nPoints = polyline.points.size();
    for( int i = 0; i < nPoints - 1; i++ ) {
      Vector2D p0 = transform(polyline.points[(i+0) % nPoints]);
      Vector2D p1 = transform(polyline.points[(i+1) % nPoints]);
      rasterize_line( p0.x, p0.y, p1.x, p1.y, c );
    }
  }
}

void SoftwareRendererImp::draw_rect( Rect& rect ) {

  Color c;
  
  // draw as two triangles
  float x = rect.position.x;
  float y = rect.position.y;
  float w = rect.dimension.x;
  float h = rect.dimension.y;

  Vector2D p0 = transform(Vector2D(   x   ,   y   ));
  Vector2D p1 = transform(Vector2D( x + w ,   y   ));
  Vector2D p2 = transform(Vector2D(   x   , y + h ));
  Vector2D p3 = transform(Vector2D( x + w , y + h ));
  
  // draw fill
  c = rect.style.fillColor;
  if (c.a != 0 ) {
    rasterize_triangle( p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, c );
    rasterize_triangle( p2.x, p2.y, p1.x, p1.y, p3.x, p3.y, c );
  }

  // draw outline
  c = rect.style.strokeColor;
  if( c.a != 0 ) {
    rasterize_line( p0.x, p0.y, p1.x, p1.y, c );
    rasterize_line( p1.x, p1.y, p3.x, p3.y, c );
    rasterize_line( p3.x, p3.y, p2.x, p2.y, c );
    rasterize_line( p2.x, p2.y, p0.x, p0.y, c );
  }

}

void SoftwareRendererImp::draw_polygon( Polygon& polygon ) {

  Color c;

  // draw fill
  c = polygon.style.fillColor;
  if( c.a != 0 ) {

    // triangulate
    vector<Vector2D> triangles;
    triangulate( polygon, triangles );

    // draw as triangles
    for (size_t i = 0; i < triangles.size(); i += 3) {
      Vector2D p0 = transform(triangles[i + 0]);
      Vector2D p1 = transform(triangles[i + 1]);
      Vector2D p2 = transform(triangles[i + 2]);
      rasterize_triangle( p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, c );
    }
  }

  // draw outline
  c = polygon.style.strokeColor;
  if( c.a != 0 ) {
    int nPoints = polygon.points.size();
    for( int i = 0; i < nPoints; i++ ) {
      Vector2D p0 = transform(polygon.points[(i+0) % nPoints]);
      Vector2D p1 = transform(polygon.points[(i+1) % nPoints]);
      rasterize_line( p0.x, p0.y, p1.x, p1.y, c );
    }
  }
}

void SoftwareRendererImp::draw_ellipse( Ellipse& ellipse ) {

  // Extra credit 

}

void SoftwareRendererImp::draw_image( Image& image ) {

  Vector2D p0 = transform(image.position);
  Vector2D p1 = transform(image.position + image.dimension);

  rasterize_image( p0.x, p0.y, p1.x, p1.y, image.tex );
}

void SoftwareRendererImp::draw_group( Group& group ) {

  for ( size_t i = 0; i < group.elements.size(); ++i ) {
    draw_element(group.elements[i]);
  }

}

// Rasterization //

// The input arguments in the rasterization functions 
// below are all defined in screen space coordinates

void SoftwareRendererImp::rasterize_point( float x, float y, Color color ) {

  // fill in the nearest pixel
  int sx = (int) floor(x);
  int sy = (int) floor(y);

  // check bounds
  if ( sx < 0 || sx >= target_w ) return;
  if ( sy < 0 || sy >= target_h ) return;

  // fill sample - NOT doing alpha blending!
  render_target[4 * (sx + sy * target_w)    ] = (uint8_t) (color.r * 255);
  render_target[4 * (sx + sy * target_w) + 1] = (uint8_t) (color.g * 255);
  render_target[4 * (sx + sy * target_w) + 2] = (uint8_t) (color.b * 255);
  render_target[4 * (sx + sy * target_w) + 3] = (uint8_t) (color.a * 255);

}

void SoftwareRendererImp::rasterize_line( float x0, float y0,
                                          float x1, float y1,
                                          Color color) {
	//1.Slope dose not exist
	//	1.1 Line lies in the window
	//	1.2 Part of the line lies in the window
	//	1.3 Line lies outside of the window
	//2.Slope exist	
	//	2.1 Line lies in the window
	//	2.2 Part of ......
	//	2.3 Line lies outside ...
	float dy = y1 - y0;
	float dx = x1 - x0;
	float slope = dy/(dx+1e-20);//Condition that the gradient does not exist.
	if(slope > 1){
		if(y0>=y1){
			float tempx = x0,tempy = y0;
			x0 = x1;
			y0 = y1;
			x1 = tempx;
			y1 = tempy;	
		}
		float epsilon = 0;
		float x = x0;
		float m = 1/slope;
		for (float y = y0;y <= y1; ++y){
				rasterize_point(x,y,color);
				if(epsilon + m<0.5){
					epsilon += m;
				}else{
					x += 1;
					epsilon = epsilon + m - 1;
				}	
		}

		return ;	

	}
	if (slope>=0){
			if(x0>=x1){
				float tempx = x0,tempy = y0;
				x0 = x1;
				y0 = y1;
				x1 = tempx;
				y1 = tempy;		
			}	
			float epsilon = 0;
			float y = y0;
			float m = slope;
			for (float x = x0; x <= x1;++x){
				rasterize_point(x,y,color);
				if (epsilon + m< 0.5){
						epsilon = epsilon + m;
				}
				else{
						y+=1;
						epsilon =   epsilon + m - 1;
				}
			}
			return;
	}

	if(slope >= -1){
			if(x0>=x1){
				float tempx = x0,tempy = y0;
				x0 = x1;
				y0 = y1;
				x1 = tempx;
				y1 = tempy;		
			}
			float epsilon = 0;
			float y = y0;
			float m = slope;
			for (float x = x0;x<=x1;++x){
				rasterize_point(x,y,color);
				if (epsilon + m > -0.5){
					epsilon = epsilon + m;
				}
				else{
					y -= 1;
					epsilon = 1 + epsilon + m;
				}
			}

		return ;
	}
	if (slope < -1){
			if(y0>=y1){
					float tempx = x0,tempy = y0;
					x0 = x1;
					y0 = y1;
					x1 = tempx;
					y1 = tempy;		
			}
			float epsilon = 0;
			float x = x0;
			float m = 1/slope;
			for (float y=y0;y<=y1;++y){
				rasterize_point(x,y,color);
				if (epsilon + m > -0.5){
						epsilon += m;
				}
				else{
						x-=1;
						epsilon = 1+epsilon + m;
				}
			}
			return ;
	}
}	

void SoftwareRendererImp::rasterize_triangle( float x0, float y0,
                                              float x1, float y1,
                                              float x2, float y2,
                                              Color color ) {
	
	//Given the counter-clockwise points X,Y,Z
	//1.calculate the perpendicular vector of XY(counter-clockwise) by multiply image i
	//  the perpendicular vector is (-(y1-y0),x1-x0);
	float xmin = x0	, xmax = x0;
	float ymin = y0 , ymax = y0;
	if ( x1 < xmin ) 
			xmin = x1;
	if ( x2 < xmin)
			xmin = x2;
	if ( x1 > xmax)
			xmax = x1;
	if ( x2 > xmax) 
			xmax = x2;
	if ( y1 < ymin )
			ymin = y1;
	if ( y2 < ymin )
			ymin = y2;
	if ( y1 > ymax )
			ymax = y1;
	if ( y2 > ymax ) 
			ymax = y2;


	/*
	float alpha = 0,bleta = 0,gama = 0;
	for (int x = xmin ; x <= xmax ;++x)
			for (int y = ymin; y <= ymax; ++y)
			{	alpha = ((y1-y2)*(x + 0.5) + (x2 - x1) * (y+0.5) + x1*y2 - x2 * y1)/((y1-y2)*x0 + (x2 - x1) * y0 + x1*y2 - x2 * y1);
				bleta = ((y2-y0)*(x + 0.5) + (x0 - x2) * (y+0.5) + x2 * y0 - x0*y2)/((y2-y0)*x1 + (x0 - x2) * y1 + x2*y0 - x0 *y2);
					gama = 1 - alpha - bleta ;
				if (alpha >= 0 && bleta >= 0 && gama >=0 )
					rasterize_point(x,y,color);	
			
			}		
	*/
		float	dx0 = x1 - x0;
		float	dx1 = x2 - x1;
		float	dx2 = x0 - x2;
		float	dy0 = y1 - y0;
		float	dy1 = y2 - y1;
		float	dy2 = y0 - y2;
	
	for(int  x = xmin -1 ; x <= xmax  + 1; ++x)
			for (int y = ymin - 1;y <= ymax  + 1;++y){

		float E0 = (x+0.5-x0) * dy0 - (y+0.5 - y0)*dx0;
		float E1 = (x+0.5-x1) * dy1 - (y+0.5 - y1)*dx1;
		float E2 = (x+0.5-x2) * dy2 - (y+0.5 - y2)*dx2;	
		if (E0<=0&&E1<=0&&E2<=0)
			rasterize_point(x,y,color);	
			}	
		

		


	  
	
		// Task 3: 
  // Implement triangle rasterization

}

void SoftwareRendererImp::rasterize_image( float x0, float y0,
                                           float x1, float y1,
                                           Texture& tex ) {
  // Task 6: 
  // Implement image rasterization

}

// resolve samples to render target
void SoftwareRendererImp::resolve( void ) {

  // Task 4: 
  // Implement supersampling
  // You may also need to modify other functions marked with "Task 4".
  return;

}


} // namespace CMU462
